/** Basic arithmetic operations */
const mylib = {
    /** Multiline arrow function. */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },

    divide: (dividend, divisor) => {
        if(divisor == 0) {
            return Error
        };
        return dividend / divisor;
    },

    /** Regular function */
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib; 