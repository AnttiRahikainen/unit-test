# Unit testing

## Contents
1. Functions
2. Unit tests

## Functions
```
add: (a, b) => {
        const sum = a + b;
        return sum;
    };
```
```
subtract: (a, b) => {
        return a - b;
    };
```
```
divide: (dividend, divisor) => {
        if(divisor == 0) {
            return Error
        };
        return dividend / divisor;
    };
```
```
multiply: function(a, b) {
        return a * b;
    };
```

## Unit tests
![unit-test](unit-test.png)
