const expect = require('chai').expect;
const mylib = require('../src/mylib')

describe("Our first unit tests", () => {
    before(() => {
        // initialization
        // create objects... etc...
        console.log("Initialising tests");
    });
    it("Can add 1 and 2 together", () => {
        // Tests
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason?");
    });
    it("Can subtract 3 and 2", () => {
        expect(mylib.subtract(3,2)).equal(1, "3 - 2 is not 1, for some reason?")
    });
    it("Cannot divide 8 and 0, without Error", () => {
        expect(mylib.divide(8,0)).equal(Error, "8 / 0 is not Error, for some reason?")
    });
    it("Can multiply 4 by 2", () => {
        expect(mylib.multiply(4,2)).equal(8, "4 * 2 is not 8, for some reason?")
    });
    after(() => {
        // Cleanup
        //For example: shut the Express server.
        console.log("Testing completed!");
    })
});